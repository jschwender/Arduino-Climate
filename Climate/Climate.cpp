/*
  Calculations of atmospheric and climate data like speed of sound, saturation pressure of water, air density and 
  pressure at NHN.
  2022-07-07 J. Schwender
*/
#include <Climate.h>
#include <natural_constants.h>
//#define DEBUG

double Climate::dewpoint(double hum, double Tcelsius) {  // K1 in hPa, K2 keine einheit, K3 °C
    const double K2 = 17.62, K3 = 243.12;
    return K3 * ( (K2*Tcelsius)/(K3+Tcelsius)+log(hum/100.0) ) / ( (K2*K3)/(K3+Tcelsius)-log(hum/100.0) );     // Magnus-Formel, -45…60 °C
}
double Climate::fh2o_satpressure(double Tabs) { // Sättigungsdampfdruck des Wassers in Pa, Tabs in Kelvin
//    return (611.657 * exp((17.62 * t)/(243.12 + t)) ); // in Pa, einfachere Formel
    return (exp(-6094.4642/Tabs+21.1249952-2.7245552/100*Tabs+1.6853396/100000*Tabs*Tabs+2.4575506*log(Tabs)) ); // das soll etwas genauer sein. https://de.wikibooks.org/wiki/Tabellensammlung_Chemie/_Stoffdaten_Wasser#S%C3%A4ttigungsdampfdruck
}
double Climate::fh2o_abshumidity(double Tabs,double rel_humidity) { // t in °C, hum in %
    return ( 10*rel_humidity * fh2o_satpressure(Tabs)/(NC_Rs_water.value * Tabs) ); // in g/m³ (Faktor 10:  /100 für % und *1000 für kg --> g)
}
double Climate::Rf(double phi, double Tabs, double p) {  // phi in % (turned into /1), returns R for humid air
    return NC_Rs_dry_air.value / (1 - (phi/100) * fh2o_satpressure(Tabs)/p * (1 - NC_Rs_dry_air.value/NC_Rs_water.value));  // psat und p in Pa, Rf in J/kg/K
}
double Climate::speedOfSound(double phi,double p, double Tabs) {  // the speed of sound deppends mainly on T, only very little on humidity and pressure
    return sqrt(1.402*Rf(phi,Tabs,p)*(Tabs));                   // 
}
double Climate::airDensity(double phi, double p, double Tabs) { // phi in %, p und psat in Pa, t in °C
    return (p / (Rf(phi,Tabs,p) * (Tabs)));   // in kg / m³
}
double Climate::T_water_boiling(double p) {  // p in Pa !!, T in °C
    return (234.175 * log(p/100.0/6.1078))/(17.08085 - log(p/100.0/6.1078) );
}

double Climate::P_NHN(double P, double QFE) {
    return  (P/pow(1-(QFE/44330.0),5.255));
}
