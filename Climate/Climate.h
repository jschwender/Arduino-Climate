#ifndef CLIMATE_h
#define CLIMATE_h
#include <math.h>

class Climate {
    public:
	double dewpoint(double hum, double Th);  // K1 in hPa, K2 keine einheit, K3 °C
	double fh2o_satpressure(double t); // Sättigungsdampfdruck des Wassers in Pa, t in °C
	double fh2o_abshumidity(double t,double hum);  // t in °C, hum in %
	double Rf(double phi, double psat, double p);  // phi in % (turned into /1), returns R for humid air
	double speedOfSound(double phi,double p, double Tabs);  // the speed of sound deppends mainly on T, only very little on humidity and pressure
	double airDensity(double phi, double p, double Tabs); // phi in %, p und psat in Pa, t in °C
	double T_water_boiling(double p); // T in °C ,  p in Pa
	double P_NHN(double P, double QFE);   // get the pressure on sea level (NHN) with pressure on a given level
};
#endif