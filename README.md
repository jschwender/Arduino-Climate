# Arduino-Climate

Calculations of atmospheric and climate data from sensor data
pressure, humidity, temperature and altitude

dewpoint, pressure at NHN, absolute humidity, air density, speed of sound, 
water boiling temperature, water saturation pressure.

requires the »natural_constants« library.

2022-07-07 J. Schwender
